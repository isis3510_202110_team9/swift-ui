# README - Gradua App

How to deploy:

1. Download the release zip.
2. Delete the Pods directory, the Podfile.lock and the gradua_app.xcworkspace file inside the gradua_app folder. 
3. Open the folder in the terminal, run:  ```pod install```. You should have Cocoa Pods installed.
4. After this process is installed, double click o the gradua_app.xcworkspace file and wait for it to open Xcode.
5. You can run the app either on your cellphone (better) or in an emulator of your choice.
6. If an error occurs, you may be asked to add a developer account and to change the Bundle Indentifier. This can be done, under the Signinf and Capabilities option under the target. Changing this, should be enough.   
