//
//  network_monitor.swift
//  gradua_app
//
//  Created by Isabela Sarmiento on 28/04/21.
//

import Foundation
import Network

class NetworkMonitor: ObservableObject {
    

        let queue = DispatchQueue( label: "Network monitor queue" )
        let monitor = NWPathMonitor()
        @Published var isConnected = true
    
        init() {
            monitor.pathUpdateHandler = { path in
                DispatchQueue.main.async {
                    self.isConnected = path.status == .satisfied ? true : false
                } }
            monitor.start( queue: queue ) }
    

    
    
}


// https://stackoverflow.com/questions/61625768/observableobject-with-nwpathmonitor





