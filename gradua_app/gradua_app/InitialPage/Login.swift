//
//  Login.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI
import Firebase
import AlertX

struct Login : View {
    
    @EnvironmentObject var session : SessionStore
    @EnvironmentObject var monitor : NetworkMonitor
    
    @Binding var click : String
    @State var email = ""
    @State var password = ""
    
    @State var showAlert = false
    @State var alertMessage = ""
    @State var isLoading = false
    @State var showAlertConnectivity = false
    
    var width : CGFloat
    
    func login() {
        isLoading = true
        
        session.signIn(email: email, password: password) { (result, error) in
            isLoading = false
            if let error = error {
                alertMessage = error.localizedDescription
                showAlert = true
                //Restart fiuelds
                password = ""
            } else {
                //Restart fields
                email = ""
                password = ""
            }
        }
        
    }
    
    func activateAllertConnectivity(){
        self.showAlertConnectivity = true
        print(showAlertConnectivity)
    }
    
    var body: some View {
        ZStack {
            VStack {
                Spacer()
                VStack(spacing: 20){
                    HStack {
                        Button(action: {click = "Initial" }) {
                            Image(systemName: "chevron.backward")
                                .foregroundColor(.gray)
                        }
                        Spacer()
                    }
                    HStack {
                        Text("Log In")
                            .font(.system(size: 48))
                            .fontWeight(.bold)
                            .foregroundColor(Color("DarkPurple"))
                        Spacer()
                    }
                    .padding(.bottom, 20)
                    //Email
                    VStack(alignment: .leading) {
                        Text("E-mail")
                            .font(.callout)
                            .fontWeight(.bold)
                            .foregroundColor(Color("DarkPurple"))
                        HStack {
                            Image(systemName: "envelope.fill").foregroundColor(.gray)
                            TextField("Enter your email...", text: $email)
                                .autocapitalization(.none)
                        }
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 15).stroke(Color.gray, lineWidth: 1))
                        
                    }
                    //Password
                    VStack(alignment: .leading) {
                        Text("Password")
                            .font(.callout)
                            .fontWeight(.bold)
                            .foregroundColor(Color("DarkPurple"))
                        HStack {
                            Image(systemName: "lock.circle.fill").foregroundColor(.gray)
                            SecureField("Enter your password...", text: $password)
                        }
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 15).stroke(Color.gray, lineWidth: 1))
                    }
                    
                    Button(action: {monitor.isConnected ? login() : activateAllertConnectivity()}) {
                        MainButton(text: "Log In", width: width/1.4)
                    }
                    .padding(.top)
                    .alertX(isPresented: $showAlert, content: {
                        AlertX(title: Text("Error"),
                               message: Text(alertMessage),
                               primaryButton: .cancel(),
                               secondaryButton: .default(Text("OK")),
                               theme: .light(withTransparency: true, roundedCorners: true))
                    })
                    .alertX(isPresented: $showAlertConnectivity, content: {
                        AlertX(title: Text("Error"),
                               message: Text("You have no connectivity, check your settings"),
                               primaryButton: .cancel(),
                               secondaryButton: .default(Text("OK")),
                               theme: .light(withTransparency: true, roundedCorners: true))
                    })
                    HStack {
                        Text("Don't have an account?")
                            .foregroundColor(.gray)
                        Button(action: {click = "Signup"})
                        {
                            Text("Create an account.")
                                .foregroundColor(monitor.isConnected ? .blue : Color("Grey"))
                        }.disabled(!monitor.isConnected)
                        Spacer()
                    }
                    .font(.body)
                    .padding(.top, 40)
                    RoundedRectangle(cornerRadius: 26)
                        .foregroundColor(Color.white)
                        .opacity(0.30)
                    
                }
                .padding(.horizontal)
                .padding(.bottom, 50)
                .padding(.top, 60)
                .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                .padding(.top, 8)
                .padding(.horizontal, 20)
                .frame(maxWidth: .infinity)
                .background(Color.white)
                .cornerRadius(30)
                .shadow(radius: 20)
                .shadow(radius: 3)
            }
            .edgesIgnoringSafeArea(.bottom)
            
            if isLoading {
                ZStack {
                    RoundedRectangle(cornerRadius: 30)
                        .foregroundColor(Color("Smoke"))
                        .opacity(0.9)
                        .shadow(radius: 10)
                        .frame(width: width/2, height: width/2)
                    Text("Loading...")
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(.gray
                        )
                }
            }
        }
        
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader {geometry in
            Login(click: .constant(""), width: geometry.size.width).environmentObject(SessionStore())
        }
    }
}
