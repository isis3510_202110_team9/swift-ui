//
//  Signup.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI
import AlertX
import Firebase

struct Signup : View {
    
    @EnvironmentObject var session : SessionStore
    @EnvironmentObject var monitor : NetworkMonitor
    
    var categories = ["Relaxation", "Insomnia", "Focus", "Stress Management", "Nature", "Daily Meditation", "Kids", "Other"]
    
    var categoriesSymbols = ["headphones", "powersleep", "eyeglasses", "moon.stars.fill", "leaf.fill", "face.smiling.fill", "gamecontroller.fill", "scribble.variable"]
    
    var columns = [GridItem(.flexible()), GridItem(.flexible())]
    
    @State var name = ""
    @State var lastName = ""
    @State var email = ""
    @State var password = ""
    @State var cPassword = ""
    @State var interests : [String] = []
    @State var termsConditions = false
    
    @State var iniSelection = [false, false, false, false, false, false, false, false]
    @State var switchPage = false
    @State var showAlert = false
    @State var alertMessage = ""
    @State var isLoading = false
    
    @State var showAlertConnectivity = false
    
    @Binding var click : String
    var width : CGFloat
    
    func generateSelection() {
        interests = []
        for (index, item) in iniSelection.enumerated() {
            if item {
                interests.append(categories[index])
            }
        }
    }
    
    func activateAllertConnectivity(){
        print("-----Alerta-----")
        self.showAlertConnectivity = true
    }
    
    func signUp() {
        isLoading = true
        
        session.signUp(email: email, password: password, name: name, lastname: lastName, interests: interests) { (result, error) in
            isLoading = false
            if let error = error {
                alertMessage = error.localizedDescription
                showAlert = true
                //Restart fiuelds
                password = ""
                cPassword = ""
            } else {
                //Restart fields
                email = ""
                password = ""
                cPassword = ""
            }
        }
        
    }
    
    var body: some View {
        ScrollView {
            VStack {
                Spacer()
                ZStack {
                    //Page 1
                    VStack(spacing: 20){
                        HStack {
                            Button(action: {click = "IniInfo" }) {
                                Image(systemName: "chevron.backward")
                                    .foregroundColor(.gray)
                            }
                            Spacer()
                        }
                        HStack {
                            Text("Create an account")
                                .font(.system(size: 48))
                                .fontWeight(.bold)
                                .fixedSize(horizontal: false, vertical: true)
                                .foregroundColor(Color("DarkPurple"))
                            Spacer()
                            
                        }
                        
                        //Name
                        VStack(alignment: .leading) {
                            Text("Name")
                                .font(.callout)
                                .fontWeight(.bold)
                                .foregroundColor(Color("DarkPurple"))
                            HStack {
                                Image(systemName: "person.fill").foregroundColor(.gray)
                                TextField("Enter your name...", text: $name)
                            }
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 15).stroke(Color.gray, lineWidth: 1))
                        }
                        
                        //LastName
                        VStack(alignment: .leading) {
                            Text("Last Name")
                                .font(.callout)
                                .fontWeight(.bold)
                                .foregroundColor(Color("DarkPurple"))
                            HStack {
                                Image(systemName: "person").foregroundColor(.gray)
                                TextField("Enter your last name...", text: $lastName)
                            }
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 15).stroke(Color.gray, lineWidth: 1))
                        }
                        
                        //email
                        VStack(alignment: .leading) {
                            Text("Email")
                                .font(.callout)
                                .fontWeight(.bold)
                                .foregroundColor(Color("DarkPurple"))
                            HStack {
                                Image(systemName: "envelope.fill").foregroundColor(.gray)
                                TextField("Enter your email address...", text: $email)
                                    .autocapitalization(.none)
                            }
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 15).stroke(Color.gray, lineWidth: 1))
                        }
                        
                        //pwd
                        VStack(alignment: .leading) {
                            Text("Password")
                                .font(.callout)
                                .fontWeight(.bold)
                                .foregroundColor(Color("DarkPurple"))
                            HStack {
                                Image(systemName: "lock.fill").foregroundColor(.gray)
                                SecureField("Enter your password...", text: $password)
                            }
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 15).stroke(Color.gray, lineWidth: 1))
                        }
                        
                        //cpwd
                        VStack(alignment: .leading) {
                            Text("Confirm Password")
                                .font(.callout)
                                .fontWeight(.bold)
                                .foregroundColor(Color("DarkPurple"))
                            HStack {
                                Image(systemName: "lock").foregroundColor(.gray)
                                SecureField("Confirm your password...", text: $cPassword)
                            }
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 15).stroke(Color.gray, lineWidth: 1))
                        }
                        
                        //Main Button Next Step
                        Button(action: {
                            if(monitor.isConnected){
                                if password != cPassword {
                                    alertMessage = "Passwords, don't match."
                                    showAlert = true
                                } else {
                                    switchPage = true
                                    showAlert = false
                                }
                            } else{
                                //activateAllertConnectivity()
                            }
                            
                        }) {
                            MainButton(text: "Next Step", width: width/1.4)
                        }
                        .disabled(monitor.isConnected == false)
                        .padding(.top)
                        .alertX(isPresented: $showAlertConnectivity, content: {
                            AlertX(title: Text("Error"),
                                   message: Text("You have no connectivity, check your settings"),
                                   primaryButton: .cancel(),
                                   secondaryButton: .default(Text("OK")),
                                   theme: .light(withTransparency: true, roundedCorners: true))
                        })
                        
                        //Already have an account
                        HStack {
                            Text("Already have an account?")
                                .foregroundColor(.gray)
                            Button(action: {click = "Login"
                                Analytics.logEvent("SignInSignUp", parameters: [
                                    "name": "SignInSignUp" as NSObject
                                ])
                            }) {
                                Text("Log in.")
                                    .foregroundColor(.blue)
                            }
                            Spacer()
                        }
                        .font(.body)
                        .padding(.top)
                        
                        
                    }
                    .offset(x: !switchPage ? 0 : -width)
                    .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                    //Page 2
                    VStack(spacing: 20) {
                        
                        //Title
                        HStack {
                            Text("Tell us about some of your interests:")
                                .font(.system(size: 25))
                                .fontWeight(.bold)
                                .fixedSize(horizontal: false, vertical: true)
                                .foregroundColor(Color("DarkPurple"))
                            Spacer()
                        }
                        
                        //Selections
                        ScrollView {
                            LazyVGrid(columns: columns, spacing: 16) {
                                ForEach(0 ..< categories.count) {index in
                                    Button(action: {
                                        iniSelection[index].toggle()
                                    }) {
                                        ZStack {
                                            Category(name: categories[index], image: categoriesSymbols[index])
                                                .cornerRadius(20)
                                                .brightness(iniSelection[index] ? -0.6 : 0)
                                            if iniSelection[index] {
                                                Image(systemName: "checkmark.circle.fill")
                                                    .foregroundColor(.white)
                                            }
                                        }
                                    }
                                }
                            }
                            .padding(.top)
                        }
                        
                        //Terms & Conditions
                        HStack(spacing: 10) {
                            Button(action: {termsConditions.toggle()}) {
                                Image(systemName: !termsConditions ? "square" : "checkmark.square.fill")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .foregroundColor(Color("DarkPurple"))
                                    .frame(width: 30)
                            }
                            Text("Click here to indicate that you have read and agree to the terms presented in the Terms and Conditions agreement.")
                                .font(.footnote)
                        }
                        
                        //Back & Next Buttons
                        HStack {
                            Button(action: {switchPage = false}) {
                                SecondaryButton(text: "Back", width: width/4)
                            }
                            Button(action: {
                                generateSelection()
                                if !monitor.isConnected {
                                    //activateAllertConnectivity()
                                }
                                else if !termsConditions {
                                    alertMessage = "You must accept the Terms & Conditions."
                                    showAlert = true
                                } else if interests.count == 0 {
                                    alertMessage = "You must select at least 1 interest."
                                    showAlert = true
                                } else {
                                    signUp()
                                }
                            }) {
                                MainButton(text: "Create account", width: width/2)
                            }
                            .disabled(monitor.isConnected == false)
                            .alertX(isPresented: $showAlert, content: {
                                AlertX(title: Text("Error"),
                                       message: Text(alertMessage),
                                       primaryButton: .cancel(),
                                       secondaryButton: .default(Text("OK")),
                                       theme: .light(withTransparency: true, roundedCorners: true))
                            })
                            .alertX(isPresented: $showAlertConnectivity, content: {
                                AlertX(title: Text("Error"),
                                       message: Text("You have no connectivity, check your settings"),
                                       primaryButton: .cancel(),
                                       secondaryButton: .default(Text("OK")),
                                       theme: .light(withTransparency: true, roundedCorners: true))
                            })
                        }
                    }
                    .offset(x: switchPage ? 0 : width)
                    .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                    
                    if isLoading {
                        ZStack {
                            RoundedRectangle(cornerRadius: 30)
                                .foregroundColor(Color("Smoke"))
                                .opacity(0.9)
                                .shadow(radius: 10)
                                .frame(width: width/2, height: width/2)
                            Text("Loading...")
                                .font(.title)
                                .fontWeight(.bold)
                                .foregroundColor(.gray
                                )
                        }
                    }
                }
                .padding(.horizontal)
                .padding(.bottom, 50)
                .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                .padding(.top, 8)
                .padding(.horizontal, 20)
                .frame(maxWidth: width)
                .background(Color.white)
                .cornerRadius(30)
                .shadow(radius: 20)
                .shadow(radius: 3)
            }
            .edgesIgnoringSafeArea(.top)
        }
        
    }
    
    struct Signup_Previews: PreviewProvider {
        static var previews: some View {
            GeometryReader {geometry in
                Signup(click: .constant("Signup"), width: geometry.size.width)
                    .environmentObject(SessionStore())
            }
        }
    }
    
    struct Category: View {
        
        var name : String
        var image : String
        
        var selected = false
        
        var body: some View {
            VStack(alignment: .leading) {
                HStack {
                    Text(name)
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                    Spacer()
                }
                Spacer()
                Image(systemName: image)
                    .foregroundColor(Color.white)
                Spacer()
            }
            .padding(16)
            .background(Color("DarkPurple").opacity(0.8).shadow(radius: 1))
            .frame(width: 150, height: 125)
        }
    }
}
