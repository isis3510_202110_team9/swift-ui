//
//  IniInfo.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI
import AlertX


struct IniInfo: View {
    
    @EnvironmentObject var monitor : NetworkMonitor
    @State var showAlertConnectivity = false
    
    @Binding var click : String
    var width : CGFloat
    
    func activateAllertConnectivity(){
        self.showAlertConnectivity = true
        print(showAlertConnectivity)
    }
    
    var body: some View {
        VStack {
            Spacer()
            VStack(spacing: 30) {
                Image("Logo")
                    .renderingMode(.template)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(Color("DarkPurple"))
                    .background(Color.white)
                    .frame(width: width/4)
                Text("Gradúa")
                    .font(.system(size: 48))
                    .fontWeight(.bold)
                    .foregroundColor(Color("DarkPurple"))
                Text("Welcome to Gradúa, your personal meditation assistant")
                    .fixedSize(horizontal: false, vertical: true)
                    .font(.body)
                    .foregroundColor(Color("DarkPurple"))
                    .padding(.top, 40)
                    .padding()
                    .multilineTextAlignment(.center)
                HStack {
                    Button(action: {monitor.isConnected ? click = "Signup" : activateAllertConnectivity()}) {
                        SecondaryButton(text: "Create an account", width: width/3)
                    }
                    .alertX(isPresented: $showAlertConnectivity, content: {
                        AlertX(title: Text("Error"),
                               message: Text("You have no connectivity, check your settings"),
                               primaryButton: .cancel(),
                               secondaryButton: .default(Text("OK")),
                               theme: .light(withTransparency: true, roundedCorners: true))
                    })
                    Button(action: {click = "Login"}) {
                        MainButton(text: "Log In", width: width/4)
                    }
                }
                
                Button(action:{click = "QuickMedi"}) {
                    Text("START QUICK MEDITATION")
                        .font(.headline)
                        .foregroundColor(Color.gray)
                        .padding()
                }
            }
            .padding(.bottom, 50)
            .padding(.top, 60)
            .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
            .padding(.top, 8)
            .padding(.horizontal, 20)
            .frame(maxWidth: .infinity)
            .background(Color.white)
            .cornerRadius(30)
            .shadow(radius: 20)
            .shadow(radius: 3)
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct IniInfo_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader {geometry in
            IniInfo(click: .constant(""), width: geometry.size.width)
        }
    }
}
