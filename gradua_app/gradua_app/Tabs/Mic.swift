//
//  Mic.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI
import AlertX
import Firebase

struct Mic : View {
    
    @EnvironmentObject var monitor : NetworkMonitor

    //Este es para mostrar el view del chat
    @State var showChat = false
    //Este es para el gesture de dismiss
    @State var chatState = CGSize.zero
    
    @State var selected = "Mic"
    @State var link = ""
    @State var meditationName = ""
    
    var width : CGFloat
    
    //Noise detection
    // 1
    @State private var mic = MicrophoneMonitor(numberOfSamples: 5)
    
    // 2
    private func normalizeSoundLevel(level: Float) -> CGFloat {
        let level = max(0.2, CGFloat(level) + 50) / 2 // between 0.1 and 25
        
        return CGFloat(level * (300 / 25)) // scaled to max at 300 (our height of our bar)
    }
    
    @State var showAlert = false
    @State var alerted = false
    @State var counter = 0
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    func noiseDetection() {
        for i in 0..<mic.soundSamples.count {
                    if self.normalizeSoundLevel(level: mic.soundSamples[i]) > 250 {
                        alerted = true
                    }
                }
    }
    
    var body: some View{
        if selected == "Player" {
            Player(select: $selected, link: link, width: width, name: meditationName, textToSpeech: false)
        } else {
            ZStack {
                //Main Content
                VStack(){
                    Spacer()
                    Button(action: {}) {
                        ZStack {
                            if !showChat
                                                        {
                                                        LottieView(name: "bgBlob")
                                                            .frame(width: width/1.3)
                                                        LottieView(name: "wave")
                                                            .frame(width: width/1.7)
                                                        }
                        }
                    }
                    
                    Spacer()
                    HStack(alignment: .center){
                        Button(action: {
                            self.showChat.toggle()
                            Analytics.logEvent("chatButtonPressed", parameters: [
                                                    "name": "chatButtonPressed" as NSObject
                                                    ])
                        }) {
                            Image("ChatBubble")
                                .shadow(radius: 7)
                        }.disabled(monitor.isConnected == false)
                        Text(monitor.isConnected ? "Can't talk? No worries, chat to us." : "Gradua is not available while you don't have connectivity")
                            .font(.body)
                            .fontWeight(.semibold)
                            .foregroundColor(.gray)
                            .multilineTextAlignment(.leading)
                    }
                    
                }
                .offset(y: showChat ? -200 : 0)
                .animation(
                    Animation
                        .default
                        .delay(0.1)
                        .speed(2)
                )
                .background(Image("BackgroundBlue")
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: width))
                //Chatbot
                ChatBot(selection: $selected, link: $link, meditationName: $meditationName, showChat: $showChat, width: width )
                    .offset(x: 0, y: showChat ? 100 : 1000)
                    .offset(y: chatState.height)
                    .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                    .gesture(
                        DragGesture()
                            .onChanged { value in
                                //aca estamos igualando el chatstate con la posicion del gesture cuando iniciamos
                                self.chatState = value.translation
                            }
                            .onEnded { value in
                                if self.chatState.height > 50 {
                                    self.showChat = false
                                }
                                else {
                                    self.chatState = .zero
                                }
                            }
                    )
            }

        }
    }
    
}

struct Mic_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader{geometry in
            Mic(width: geometry.size.width)
        }
    }
}
