//
//  Home.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI
import AVKit
import Firebase

struct Home : View {
    
    @State var audioPlayer: AVAudioPlayer!
    var geo : GeometryProxy
    @Binding var player : Bool
    @EnvironmentObject var session : SessionStore
    @EnvironmentObject var monitor : NetworkMonitor
    
    @State var populars : [MeditationBack] = []
    @State var catalog : [MeditationBack] = []
    @State var favorites : [MeditationBack] = []
    @State var recents : [MeditationBack] = []
    @State var meditation : MeditationBack?
    
    func getPopulars()
    {
        self.populars = session.meditations.sorted{
            $0.reproductions > $1.reproductions
        }
    }
    
    func getCatalog()
    {
        self.catalog = session.meditations
    }
    
    func updateMeditation(meditation : MeditationBack)
    {
        session.updateMeditation(meditation: meditation)
    }
    
    func createFavorites() {
        /* if let favs = UserDefaults.standard.data(forKey: "userFavorites") {
         if let decodedFAV = try? JSONDecoder().decode([MeditationBack].self, from: favs) {
         print("Retrieving favorites...")
         self.favorites = decodedFAV
         }
         } else {*/
        for fav in session.profile?.favorites ?? [] {
            session.getMeditation(name: fav) {(meditation, error) in
                self.favorites.append(meditation!)
            }
        }
        save()
        //}
    }
    
    func getAndUpdateMeditation (name: String)
    {
        session.getMeditation(name: name){(meditation, error) in
            self.meditation = meditation
            self.meditation?.reproductions = meditation!.reproductions + 1
            updateMeditation(meditation : self.meditation!)
        }
    }
    
    func getRecents() {
        if let recs = UserDefaults.standard.data(forKey: "userRecents") {
            if let decodedREC = try? JSONDecoder().decode([MeditationBack].self, from: recs) {
                print("Retrieving recents...")
                self.recents = decodedREC
            }
        } else {
            save()
        }
    }
    
    func registerRecent(meditation: MeditationBack) {
        if !self.recents.contains(meditation) {
            self.recents.append(meditation)
        }
        save()
    }
    
    func save() {
        if let encodedFAV = try? JSONEncoder().encode(self.favorites) {
            UserDefaults.standard.set(encodedFAV, forKey: "userFavorites")
            print("Successfully Saved Favorites")
        }
        if let encodedREC = try? JSONEncoder().encode(self.recents) {
            UserDefaults.standard.set(encodedREC, forKey: "userRecents")
            print("Successfully Saved Recents")
        }
    }
    
    var body: some View {
        NavigationView {
            ScrollView {
                ZStack{
                    RoundedRectangle(cornerRadius: 20)
                        .foregroundColor(monitor.isConnected ? Color("Purple") : Color("Grey"))
                        .opacity(0.30)
                    VStack {
                        VStack(alignment: .leading, spacing: 10) {
                            Text("Meditations")
                                .font(.largeTitle)
                                .fontWeight(.bold)
                            Text("According to your emotions, goals and interests")
                                .font(.body)
                        }
                        .padding(.horizontal,24 )
                        .padding(.bottom, 17)
                        NavigationLink(destination: Player(select: .constant(""), link: "", width: geo.size.width, name: "New Generated Meditation", textToSpeech: true).navigationBarBackButtonHidden(true)) {
                            SecondaryButton(text: "Create mediation", width: geo.size.width/2.5)
                        }
                        .simultaneousGesture(TapGesture().onEnded({
                            self.session.add_generada()
                        }))
                        .disabled(!monitor.isConnected )
                    }
                    .padding()
                }
                .padding()
                //.frame(maxWidth: .infinity)
                
                HStack {
                
                    Text("Favorites")
                        .font(.title)
                  
                    Spacer()
                }
                .padding(.horizontal)
                if favorites.count == 0
                {
                    Text("You have not added favorites yet")
                        .font(.body)
                }
                ScrollView (.horizontal, showsIndicators: false){
                    HStack {
                        ForEach(favorites, id: \.id) { recents in
                            NavigationLink(destination: Player(select: .constant(""), link: recents.link, width: geo.size.width, name: recents.label, textToSpeech: false))  {
                                MeditationCard(meditation: recents, width: geo.size.width, isFavorite: true)
                            }.simultaneousGesture(TapGesture().onEnded({
                                self.getAndUpdateMeditation(name: recents.label.lowercased())
                                print("Reproduction Counted")
                                registerRecent(meditation: recents)
                                Analytics.logEvent("playFavoritesMeditation", parameters: [
                                    "name": "playFavoritesMeditation" as NSObject
                                ])
                            }))
                            
                        }
                    }
                }
                HStack {
                    Text("Populars")
                        .font(.title)
                    Spacer()
                }
                .padding(.horizontal)
                ScrollView (.horizontal, showsIndicators: false){
                    HStack {
                        ForEach(0 ..< self.populars.count, id: \.self){ index in
                            NavigationLink(destination: Player(select: .constant(""), link: populars[index].link, width: geo.size.width, name: populars[index].label, textToSpeech: false))  {
                                MeditationCard(meditation: populars[index], width: geo.size.width, isFavorite: false)
                            }
                            .disabled(monitor.isConnected == false)
                            .simultaneousGesture(TapGesture().onEnded({
                                let name = self.populars[(index)].label
                                self.getAndUpdateMeditation(name: name.lowercased())
                                print("Reproduction Counted")
                                registerRecent(meditation: populars[index])
                                Analytics.logEvent("playPopularMeditation", parameters: [
                                    "name": "playPopularMeditation" as NSObject
                                ])
                            }))
                            
                        }
                    }
                }
                HStack {
                    Text("All Meditations")
                        .font(.title)
                    Spacer()
                }
                .padding(.bottom)
                .padding(.horizontal)
                LazyVStack (spacing: 60){
                    ForEach(catalog, id: \.id) { item in
                        NavigationLink(destination: Player(select: .constant(""), link: item.link, width: geo.size.width, name: item.label, textToSpeech: false))  {
                            MeditationFullWidthCard(meditation: item, isFavorite: false, geo: geo)
                        }.simultaneousGesture(TapGesture().onEnded({
                            self.getAndUpdateMeditation(name: item.label.lowercased())
                            print("Reproduction Counted")
                            registerRecent(meditation: item)
                            Analytics.logEvent("playFavoritesMeditation", parameters: [
                                "name": "playFavoritesMeditation" as NSObject
                            ])
                        }))
                        
                    }
                }
            }
            .font(.largeTitle)
            .background(Image("homeBG")
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: geo.size.width))
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
        .onAppear(perform: {
            self.populars = []
            self.favorites = []
            self.recents = []
            self.catalog = []
            self.getPopulars()
            self.createFavorites()
            self.getRecents()
            self.getCatalog()
        })
    }
}

/*
 ESTA PARTE DEL PREVIEW ES SOLO CUANDO SEPARAMOS EL SUBVIEW EN UN ARCHIVO A PARTE PARA PODER VISUALIZARLO SOLITARIO EN EL CANVAS SIN TENER QUE VERLO MEZCLADO CON TODA LA APP.
 
 NOTA: SI A HOME LE AGREGAN MAS PARAMETROS TAMBIEN TENDRAN QUE AGREGARSELO A ESTA PARTE COMO SI FUERAN PRUEBAS UNITARIAS... LE MANDAN PARAMETROS DUMMIES PARA VERLO EN ESTA PANTALLA, SIN EMBARGO, CUANDO LO AGREGUEN A CONTENT VIEW SI DEBERAN LLENAR LOS PARAMETROS CON LOS DATOS REALES QUE SE NECESITAN.
 */
struct Home_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader{geometry in
            Home(geo: geometry, player: .constant(true)).environmentObject(SessionStore())
        }
    }
}

struct MeditationCard: View {
    
    @EnvironmentObject var monitor : NetworkMonitor
    @State var meditation : MeditationBack
    @State var width : CGFloat
    @State var isFavorite : Bool
    var body: some View {
        
        ZStack{
            RoundedRectangle(cornerRadius: 26)
                .foregroundColor( monitor.isConnected || isFavorite ? Color("Purple") : Color("Grey"))
                .opacity(0.30)
            HStack {
                VStack(alignment: .leading, spacing: 0){
                    Text(meditation.label)
                        .font(.title3)
                        .foregroundColor(monitor.isConnected || isFavorite ? Color("Purple") : Color("Grey"))
                    Spacer()
                    Text("\(Int.random(in: 2..<10)) min")
                        .font(.body)
                        .foregroundColor(.white)
                    
                }
                
                Spacer()
                Image(meditation.label)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(monitor.isConnected || isFavorite ? Color("Purple") : Color("Grey"))
                    .frame(width: width/6)
            }.padding()
        }
        .padding(.horizontal)
        .frame(width: width/1.8, height: 130)
        
        
    }
}

struct MeditationFullWidthCard: View {
    
    @EnvironmentObject var monitor : NetworkMonitor
    @State var meditation : MeditationBack
    @State var isFavorite : Bool
    @State var geo : GeometryProxy
    var body: some View {
        
            ZStack{
                RoundedRectangle(cornerRadius: 26)
                    .foregroundColor( monitor.isConnected || isFavorite ? Color("Purple") : Color("Grey"))
                    .opacity(0.30)
                HStack {
                    VStack(alignment: .leading, spacing: 0){
                        Text(meditation.label)
                            .font(.title3)
                            .foregroundColor(monitor.isConnected || isFavorite ? Color("Purple") : Color("Grey"))
                        Text("It is a long established fact that a reader will be distracted by the readable content.")
                            .font(.caption)
                            .foregroundColor(monitor.isConnected || isFavorite ? Color("Grey") : Color(.white))
                            .fixedSize(horizontal: false, vertical: true)
                            .padding(.top)
                        Spacer()
                        Text("\(Int.random(in: 2..<10)) min")
                            .font(.body)
                            .foregroundColor(.white)
                        
                    }
                    
                    Spacer()
                    Image(meditation.label)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(monitor.isConnected || isFavorite ? Color("Purple") : Color("Grey"))
                        .frame(width: .infinity)
                }.padding()
            }
            .padding(.horizontal)
            .frame(width: .infinity, height: geo.size.height * 0.15)
    }
}
