//
//  Profile.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI
import Firebase
import Foundation

struct Profile : View {
    
    @EnvironmentObject var session : SessionStore
    @EnvironmentObject var monitor : NetworkMonitor
    @EnvironmentObject var assistant : AssistantManager
    
    
    var width : CGFloat
    
    func time_format(seconds: Int)->String{
        print("SECOOOONDS")
        print(seconds)
        var hours_string = "00"
        var minutes_string = "00"
        var seconds_string = "00"
        var minutes = 0;
        if(seconds > 60){
            seconds_string = String(format: "%02d",seconds%60)
            minutes = Int(seconds/60)
            if(minutes>60){
                minutes_string = String(format: "%02d",Int(minutes%60))
                hours_string = String(format: "%02d",minutes/60)
            }
            else{
                minutes_string = String(format: "%02d",minutes)
            }
        }
        else{
            seconds_string = String(format: "%02d",seconds)
        }
        return hours_string + ":" + minutes_string + ":" + seconds_string
    }
    
    var body: some View {
        GeometryReader {geometry in
            ZStack {
                ScrollView{
                    VStack(spacing : 40){
                        VStack {
                            //Botones arriba
                            HStack{
                                Spacer()
                                Button(action: {session.signOut()}){
                                    Image(systemName: "arrow.right.to.line")
                                        .font(.system(.title))
                                }
                                .disabled(monitor.isConnected == false)
                            }
                            .foregroundColor(.gray)
                            .padding()
                            VStack(spacing: 10){
                                Button(action: {}){
                                    Image(systemName: "person")
                                        .font(.system(size: 70))
                                        .foregroundColor(.white)
                                        .background(
                                            Circle(
                                            ).fill(Color("Ultramarine Blue"))
                                            .frame(width: 86, height: 86))
                                }
                                Text("\(session.profile?.name ?? "") \(session.profile?.lastname ?? "")")
                                    .font(.system(.title))
                                // Boton para generar una falla
                                /*
                                 Button(action: {fatalError()}) {
                                 Text("Crash")
                                 }.disabled(monitor.isConnected == false)
                                 Text( monitor.isConnected ? "connected :)": "not connected :(" ).padding()
                                 */
                            }
                        }
                        .padding(.bottom)
                        //Meditaciones
                        ZStack{
                            RoundedRectangle(cornerRadius: 26)
                                .foregroundColor(Color("Grey"))
                                .opacity(0.30)
                            VStack {
                                HStack {
                                    Image("meditacionesIcon")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height: geometry.size.height * 0.05)
                                    Text("Meditations")
                                        .font(.title3)
                                        .foregroundColor(Color.black)
                                    Spacer()
                                }
                                HStack {
                                    Text("Sessions: ")
                                        .foregroundColor(Color("Grey"))
                                    //TODO RECUENTO DE NUMERO DE SESIONES...S
                                    Text("\(String(session.stats?.num_meditaciones ?? 0))")
                                        .foregroundColor(Color.black)
                                        .padding(.trailing)
                                    
                                    Text("Time: ")
                                        .foregroundColor(Color("Grey"))
                                    //TODO TIEMPO
                                    Text("\(time_format(seconds: session.stats?.tiempo ?? 0))")
                                        .foregroundColor(Color.black)
                                        .padding(.trailing)
                                }
                                .font(.title2)
                                Spacer()
                            }
                            .padding()
                        }
                        .padding(.horizontal)
                        .frame(width: .infinity, height: abs(geometry.size.height * 0.15))
                        .padding(.bottom)
                        
                        //Sentimientos
                        ZStack{
                            RoundedRectangle(cornerRadius: 26)
                                .foregroundColor(Color("Grey"))
                                .opacity(0.30)
                            VStack {
                                HStack {
                                    Image("smileyIcon")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height: geometry.size.height * 0.05)
                                    Text("Emotions")
                                        .font(.title3)
                                        .foregroundColor(Color.black)
                                    Spacer()
                                }
                                HStack {
                                    VStack {
                                        HStack {
                                            Text("Positive: ")
                                                .foregroundColor(Color("Grey"))
                                            //TODO NUMERO POSITIVOS
                                            Text("\(String(assistant.messagesStats.positive_emotions))")
                                                .foregroundColor(Color.black)
                                                .padding(.trailing)
                                            
                                            Text("Negative: ")
                                                .foregroundColor(Color("Grey"))
                                            //TODO NUMERO NEGATIVOS
                                            Text("\(String(assistant.messagesStats.negative_emotions))")
                                                .foregroundColor(Color.black)
                                                .padding(.trailing)
                                        }
                                        
                                        
                                        
                                    }
                                    Spacer()
                                }
                                .font(.title2)
                                Spacer()
                            }
                            .padding()
                        }
                        .padding(.horizontal)
                        .frame(width: .infinity, height: abs(geometry.size.height * 0.15))
                        .padding(.bottom)
                        
                        //Mensajes Gradua
                        ZStack{
                            RoundedRectangle(cornerRadius: 26)
                                .foregroundColor(Color("Grey"))
                                .opacity(0.30)
                            VStack {
                                HStack {
                                    Image(systemName: "message.fill")
                                        .font(.system(size: geometry.size.height * 0.03))
                                        .foregroundColor(.white)
                                        .background(
                                            Circle(
                                            ).fill(Color("Ultramarine Blue"))
                                            .frame(width: geometry.size.height * 0.05, height: geometry.size.height * 0.05))
                                    Text("Messages")
                                        .font(.title3)
                                        .foregroundColor(Color.black)
                                    Spacer()
                                }
                                Spacer()
                                HStack {
                                    VStack {
                                        HStack {
                                            //TODO NUMERO DE MENSAJES
                                            Spacer()
                                            Text("\(String(assistant.messages.count))")
                                                .foregroundColor(Color.black)
                                                .padding(.trailing)
                                            Spacer()
                                        }
                                    }
                                    Spacer()
                                }
                                .font(.title2)
                                Spacer()
                            }
                            .padding()
                        }
                        .padding(.horizontal)
                        .frame(width: .infinity, height: abs(geometry.size.height * 0.15))
                        .padding(.bottom)
                        
                        //Mensajes Gradua
                        ZStack{
                            RoundedRectangle(cornerRadius: 26)
                                .foregroundColor(Color("Grey"))
                                .opacity(0.30)
                            VStack {
                                HStack {
                                    Image("wandIcon")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height: geometry.size.height * 0.05)
                                    Text("Created meditations")
                                        .font(.title3)
                                        .foregroundColor(Color.black)
                                    Spacer()
                                }
                                Spacer()
                                HStack {
                                    VStack {
                                        HStack {
                                            //TODO NUMERO DE MENSAJES
                                            Spacer()
                                            Text("\(String(session.stats?.num_generadas ?? 0))")
                                                .foregroundColor(Color.black)
                                                .padding(.trailing)
                                            Spacer()
                                        }
                                    }
                                    Spacer()
                                }
                                .font(.title2)
                                Spacer()
                            }
                            .padding()
                        }
                        .padding(.horizontal)
                        .frame(width: .infinity, height: abs(geometry.size.height * 0.15))
                        .padding(.bottom)
                        
                    }
                    .background(Color.white)
                }
            }
        }
    }
    
}

struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader {geometry in
            Profile(width: geometry.size.width).environmentObject(SessionStore())
        }
    }
}
