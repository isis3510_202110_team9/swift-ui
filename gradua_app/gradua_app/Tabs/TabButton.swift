//
//  TabButton.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI

//Aca ya creamos cada componente de la barra de navegacion

struct TabButton : View {
    
    var image : String
    @Binding var selectedTab : String
    var width, height: CGFloat
    
    var body: some View {
        
        Button(action: {selectedTab = image}){
            //Hacerle lo del rendering mode es para poderle cambiar el color al .svg
            Image(image)
                .resizable()
                .renderingMode(.template)
                .aspectRatio(contentMode: .fit)
                .frame(width: width, height: height)
                .padding(.horizontal)
                .foregroundColor(selectedTab == image ? Color("Selection") : Color.black.opacity(0.4))
            
        }
        
    }
}
