//
//  Player.swift
//  gradua_app
//
//  Created by Juan Camilo Garcia on 31/03/21.
//

import SwiftUI
import AVKit
import AlertX
import Firebase

struct Player: View {
    
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    @EnvironmentObject var session : SessionStore
    @EnvironmentObject var api : GenMeditationsAPI
    @State var audioPlayer: AVAudioPlayer!
    @State var synthesizer: AVSpeechSynthesizer!
    @State var utterance: AVSpeechUtterance!
    @State var isPlaying : Bool = true
    @Binding var select : String
    @State var link : String
    @State var width : CGFloat
    @State var name : String
    @State var textToSpeech : Bool
    @State var isFavorite : Bool = false
    @State var isFavoriteIMG = "suit.heart"
    @State var isDownloaded : Bool = false
    @State var isDownloadedIMG = "arrow.down.circle.fill"
    @EnvironmentObject var monitor : NetworkMonitor
    @State var errorDownloading = false
    @State var fileSize : String = ""
    @State var fileSizeColor : Color = Color(#colorLiteral(red: 0.5019607843, green: 1, blue: 0.8588235294, alpha: 1))
    @State var avblStorage : String = ""
    @State var storageColor : Color = Color(#colorLiteral(red: 0.5019607843, green: 1, blue: 0.8588235294, alpha: 1))
    @State var showNameInput : Bool = false
    @State var generatedMeditation : GeneratedMeditation!
    
    @State var loud : Bool = false
    //Noise detection
    // 1
    @ObservedObject private var mic = MicrophoneMonitor(numberOfSamples: 5)
    
    // 2
    private func normalizeSoundLevel(level: Float) -> CGFloat {
        let level = max(0.2, CGFloat(level) + 50) / 2 // between 0.1 and 25
        print(level)
        if level >= 1
        {
            loud = true
        }
        else
        {
            loud = false
        }
        
        return CGFloat(level * (300 / 200)) // scaled to max at 300 (our height of our bar)
    }
    
    @State var showDb = false
    @State var alerted = false
    @State var counter = 0
    @State var db = CGFloat(0)
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    func noiseDetection()  {
        showDb = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            showDb = false
        }
    }
    
    func addToFavorites() {
        if !session.profile!.favorites.contains(self.name.lowercased()) {
            session.profile?.favorites.append(self.name.lowercased())
            self.isFavorite = true
            self.download()
        } else {
            if let index = session.profile?.favorites.firstIndex(of: self.name.lowercased()) {
                session.profile?.favorites.remove(at: index)
            }
            self.isFavorite = false
            self.deleteDownload()
        }
        session.updateUserProfile(user: session.profile!)
    }
    
    func download() {
        if let audioUrl = URL(string: self.link) {
            
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print("Destination URL: \(destinationUrl)")
            
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                self.isDownloaded = true
                
            } else {
                print("NO EXISTE, CREEMOSLO")
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    print("ENTRO ANTES DEL GUARD")
                    guard let location = location, error == nil else { return }
                    do {
                        print("PREVIO A MOVER EL ITEM DE \(location) a \(destinationUrl)")
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder")
                        self.isDownloaded = true
                        self.errorDownloading = false
                    } catch let error as NSError {
                        self.errorDownloading = true
                        print(error.localizedDescription)
                    }
                }).resume()
            }
            //Set File Size String
            self.fileSize = fileSizeToString(with: getFileSize(path: destinationUrl))
            print("File size calculated: \(self.fileSize)")
            
            //Set label color based on file size
            let split = self.fileSize.components(separatedBy: " ")
            let number = Double(split[0])
            if self.fileSize.contains("MB") {
                self.fileSizeColor = number! <= 3.0 ? Color(#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)) : Color(#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1))
            }
            if self.fileSize.contains("GB") {
                self.fileSizeColor = Color(#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1))
            }
            
            //Set available storage
            self.avblStorage = fileSizeToString(with: getAvailableStorage())
            let splitStorage = self.avblStorage.components(separatedBy: " ")
            let strg = Double(splitStorage[0])!
            if strg < 10.0 {
                self.storageColor = Color(#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1))
            } else if strg < 3.0 {
                self.storageColor = Color(#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1))
            }
            
        }
    }
    
    func getFileSize(path: URL) -> UInt64 {
        do {
            let attributes = try FileManager.default.attributesOfItem(atPath: path.path)
            if let fileSize = attributes[FileAttributeKey.size]  {
                return (fileSize as! NSNumber).uint64Value
            } else {
                print("Failed to get a size attribute from path: \(path)")
            }
        } catch {
            print("Error: \(error)")
        }
        return 0
    }
    
    func fileSizeToString(with size: UInt64) -> String {
        var conversion = Double(size)
        var mFactor = 0
        let tokens = ["bytes", "KB", "MB", "GB", "TB", "PB",  "EB",  "ZB", "YB"]
        while conversion > 1024 {
            conversion /= 1024
            mFactor += 1
        }
        return String(format: "%4.2f %@", conversion, tokens[mFactor])
    }
    
    /*
     https://stackoverflow.com/questions/26198073/query-available-ios-disk-space-with-swift
     Total available capacity in bytes for "Important" resources, including space expected to be cleared by purging non-essential and cached resources. "Important" means something that the user or application clearly expects to be present on the local system, but is ultimately replaceable. This would include items that the user has explicitly requested via the UI, and resources that an application requires in order to provide functionality.
     Examples: A video that the user has explicitly requested to watch but has not yet finished watching or an audio file that the user has requested to download.
     This value should not be used in determining if there is room for an irreplaceable resource. In the case of irreplaceable resources, always attempt to save the resource regardless of available capacity and handle failure as gracefully as possible.
     */
    func getAvailableStorage() -> UInt64 {
        if #available(iOS 11.0, *) {
            if let space = try? URL(fileURLWithPath: NSHomeDirectory() as String).resourceValues(forKeys: [URLResourceKey.volumeAvailableCapacityForImportantUsageKey]).volumeAvailableCapacityForImportantUsage {
                return UInt64(space)
            } else {
                return 0
            }
        } else {
            if let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
               let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value {
                return UInt64(freeSpace)
            } else {
                return 0
            }
        }
    }
    
    func deleteDownload() {
        let audioUrl = URL(string: self.link)!
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
        do {
            try FileManager.default.removeItem(at: destinationUrl)
            self.isDownloaded = false
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func generateMeditation(username: String) {
        api.getMeditation(username: username.replacingOccurrences(of: " ", with: "+")) { text, err in
            if let error = err {
                print(error)
            } else {
                self.generatedMeditation = text
                self.utterance = AVSpeechUtterance(string: self.generatedMeditation.meditation)
                utterance.voice = AVSpeechSynthesisVoice(language: "es-MX")
                utterance.rate = 0.4
                
                self.synthesizer = AVSpeechSynthesizer()
            }
        }
    }
    
    var body: some View {
        VStack(alignment: .center){
            HStack(){
                Spacer()
                Button(action: {
                    if !textToSpeech {
                        self.audioPlayer.pause()
                    } else {
                        synthesizer.pauseSpeaking(at: AVSpeechBoundary.immediate)
                    }
                    if session.session != nil {
                        select = "Mic"
                    } else {
                        select = "IniInfo"
                    }
                    self.mode.wrappedValue.dismiss()
                }){
                    Image(systemName: "xmark")
                        .font(.system(.title))
                }
            }
            .padding(.horizontal, 35)
            .foregroundColor( Color("Grey"))
            
            VStack {
                HStack {
                    Text("You're listening to: \(self.name)")
                        .foregroundColor(Color("ChatBubble"))
                }
                if self.isDownloaded {
                    HStack {
                        Image(systemName: self.isDownloadedIMG)
                            .foregroundColor(Color(#colorLiteral(red: 0.5019607843, green: 1, blue: 0.8588235294, alpha: 1)))
                        Text(self.fileSize)
                            .foregroundColor(self.fileSizeColor)
                        Text("Avbl Storage: \(self.avblStorage)")
                            .foregroundColor(self.storageColor)
                    }
                }
            }
            
            Spacer()
          
          
            Button(action: {}) {
                ZStack {
                    LottieView(name: "bgBlob")
                        .frame(width: width/1.3)
                    LottieView(name: "wave")
                        .frame(width: width/1.7)
                }
            }
            Spacer()
            HStack(){
                //Esta expression se resumen en que se va a usar una meditacion de un link ya que se esta loggeado y no es t2s
                if session.session != nil && !textToSpeech {
                    Button(action: {
                        self.addToFavorites()
                        self.isFavoriteIMG = self.isFavorite ? "suit.heart.fill" : "suit.heart"
                        Analytics.logEvent("savedAsFavorite", parameters: [
                            "name": "savedAsFavorite" as NSObject
                        ])
                    }){
                        Image(systemName: self.isFavoriteIMG)
                            .font(.system(.title))
                    }
                    .disabled(!monitor.isConnected && !isFavorite)
                }
                Spacer()
                Button(action: {
                    if session.session != nil && !textToSpeech {
                        if self.isPlaying {
                            self.audioPlayer.play()
                            Analytics.logEvent("playButtonPressed", parameters: [
                                "name": "playButtonPressed" as NSObject
                            ])
                            print("IS PLAYING")
                            self.session.add_num_meditaciones()
                            self.session.add_tiempo()
                        }
                        else{
                            self.audioPlayer.pause()
                            Analytics.logEvent("pauseButtonPressed", parameters: [
                                "name": "pauseButtonPressed" as NSObject
                            ])
                        }
                    } else {
                        if synthesizer!.isSpeaking {
                            if synthesizer.isPaused {
                                synthesizer.continueSpeaking()
                                
                            } else {
                                synthesizer.pauseSpeaking(at: AVSpeechBoundary.immediate)
                            }
                        } else {
                            synthesizer.speak(utterance)
                            self.session.add_num_meditaciones()
                            self.session.add_tiempo()
                        }
                    }
                    self.isPlaying.toggle()
                }){
                    Image(systemName: self.isPlaying == true ? "play.circle" : "pause.circle")
                        .font(.system(size: 60))
                }
                
                //TODO
                Spacer()
                VStack {
               
                           if showDb {
                            HStack(spacing: 2) {
                                             // 4
                                            ForEach(mic.soundSamples, id: \.self) { level in
                                                ZStack {
                                                   // 2
                                                    RoundedRectangle(cornerRadius: 20)
                                                        .fill(loud ? Color.red: Color("Grey"))
                                                        // 3
                                                        .frame(width: 3, height:  self.normalizeSoundLevel(level: level))
                                                }
                                            }

                                        }
                            Text(String(format: "%02d", Int(self.normalizeSoundLevel(level: mic.soundSamples[3]))))

                           }
                           else{
                            Button(action: {
                                noiseDetection()
                            }){
                                Image(systemName: "mic.circle.fill")
                                    .font(.system(.title))
                            }
                            
                           }
                       }
            }.padding(.horizontal, 40)
            .foregroundColor( Color("Grey"))
            Spacer()
        }
        .padding(.bottom)
        .padding(.top, session.session == nil ? 40 : 0)
        .background(Image("PlayerBG")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: width))
        .frame(maxWidth: .infinity)
        .onAppear{
            //If we are logged in and no t2s = normal link player
            let audioSession = AVAudioSession.sharedInstance()
            
            do {
                try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
            } catch let error as NSError {
                print("audioSession error: \(error.localizedDescription)")
            }
            
            if session.session != nil && !textToSpeech {
                self.isFavorite = session.profile!.favorites.contains(self.name.lowercased())
                self.isFavoriteIMG = self.isFavorite ? "suit.heart.fill" : "suit.heart"
                
                if self.isFavorite {
                    self.download()
                    if let audioUrl = URL(string: self.link) {
                        
                        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                        
                        let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
                        do {
                            self.audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                            guard let player = audioPlayer else { return }
                            
                            player.prepareToPlay()
                        } catch let error {
                            print(error.localizedDescription)
                        }
                    }
                } else {
                    let fileURL = NSURL(string: self.link)
                    let soundData = NSData(contentsOf:fileURL! as URL)
                    self.audioPlayer = try! AVAudioPlayer(data: soundData! as Data)
                }
            }
            else {
                //This condition is implicit for text2speech
                generateMeditation(username: session.session != nil ? (session.profile?.name ?? "") : "Nuevo usuario")
            }
        }

    }
}
