//
//  ContentView.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 25/03/21.
//

import SwiftUI

struct ContentView: View {
    //Add this line in the view from which you want to access the user... remember to add it to the preview as an environmentObject(SessionStore())
    @EnvironmentObject var session : SessionStore
    @EnvironmentObject var monitor : NetworkMonitor
    
    
    func getUser() {
        session.listen()
    }
    
    var body: some View {
        Group {
            if session.session != nil {
                CustomTabView()
            } else {
                Initial()
            }
        }.onAppear(perform: {
            getUser()
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(SessionStore())
    }
}

/*
 NOOOOO TOCAAAAAARRRRR
 */
//Primero creamos las variables del nombre de los iconos
var tabs = ["Circle", "Mic", "User"]
struct CustomTabView : View {
    
    @State var selected = "Mic"
    @State var showPlayer = true
    
    @EnvironmentObject var monitor : NetworkMonitor
    @State private var hasTimeElapsed = false
    @State private var hasTimeElapsed2 = true
    @State private var initialCase = false
    
    
    var height = CGFloat(400.0)
    
    func iniShowPlayer() {
        showPlayer = true
    }
    
    func delayAllert() {
        // Delay of 5 seconds
        hasTimeElapsed = false
        initialCase = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            hasTimeElapsed = true
        }
    }
    
    func delayAllert2() {
        // Delay of 5 seconds
        hasTimeElapsed2 = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            hasTimeElapsed2 = true
        }
    }
    
    var body: some View{
        //Con el geometry reader podemos saber en todas partes el tama;o de la pantalla relativa al dispositivo.
        GeometryReader {geometry in
            ZStack {
                VStack {
                    switch selected {
                    case "Circle":
                        Home(geo: geometry, player: $showPlayer)
                    case "Mic":
                        Mic(width: geometry.size.width)
                    case "User":
                        Profile(width: geometry.size.width)
                        Spacer()
                    default:
                        Home(geo: geometry, player: $showPlayer)
                    }
                    //Esta parte solo nos proporciona el cambio de views, nada de inconos ni nada visible.
                    /*TabView(selection: $selected){
                     Home(width: geometry.size.width)
                     .tag("Circle")
                     Mic(width: geometry.size.width)
                     .tag("Mic")
                     Profile(width: geometry.size.width)
                     .tag("User")
                     }
                     .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))*/
                    //Aca ya creamos la barra de navegacion
                    if showPlayer {
                        HStack(){
                            
                            ForEach(tabs,id: \.self){image in
                                
                                TabButton(image: image, selectedTab: $selected, width: geometry.size.width/5, height: geometry.size.height/28)
                                
                            }
                            
                        }
                        .frame(width: geometry.size.width, height: geometry.size.height/8)
                        .background(Color.white
                                        .shadow(radius: 2))
                    }
                    //Esto ultimo es por si sale el teclado, que no se suba hasta arriba el nav.
                }
                .edgesIgnoringSafeArea(.bottom)
                VStack {
                    Spacer()
                    ZStack {
                        RoundedRectangle(cornerRadius: 20)
                            .foregroundColor(Color.black)
                            .opacity(0.95)
                        HStack {
                            Image(systemName: "exclamationmark.circle")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: geometry.size.width/10)
                                .foregroundColor(.white)
                            VStack(alignment: .leading, spacing: 0){
                                Text("You lost internet connection!")
                                    .font(.title3)
                                    .foregroundColor(Color.white)
                                Text("Please check your settings")
                                    .font(.body)
                                    .foregroundColor(Color.white)
                            }
                            .padding(.horizontal)
                        }
                    }
                    .frame(height: height/6)
                    .offset(x: 0, y: monitor.isConnected || hasTimeElapsed ? +300 : -70)
                    .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                    .padding()
                    .onReceive(monitor.$isConnected){ _ in
                        DispatchQueue.main.async {
                            if !monitor.isConnected{
                                delayAllert()
                            }
                        }
                    }
                }
                VStack {
                    Spacer()
                    ZStack {
                        RoundedRectangle(cornerRadius: 20)
                            .foregroundColor(Color.black)
                            .opacity(0.95)
                        HStack {
                            Image(systemName: "exclamationmark.circle")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: geometry.size.width/10)
                                .foregroundColor(.white)
                            VStack(alignment: .leading, spacing: 0){
                                Text("Connection restored")
                                    .font(.title3)
                                    .foregroundColor(Color.white)
                            }
                            .padding(.horizontal)
                        }
                    }
                    .frame(height: height/6)
                    .offset(x: 0, y: monitor.isConnected && !hasTimeElapsed2 ? -70 : +300)
                    .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                    .padding()
                    .onReceive(monitor.$isConnected){ _ in
                        DispatchQueue.main.async {
                            if monitor.isConnected && initialCase{
                                delayAllert2()
                            }
                        }
                    }
                }
            }
        }
    }
}

struct Initial : View {
    
    @State var current = "Initial"
    
    @EnvironmentObject var monitor : NetworkMonitor
    @State private var hasTimeElapsed = false
    @State private var hasTimeElapsed2 = true
    @State private var initialCase = false
    var height = CGFloat(400.0)
    
    func delayAllert() {
        // Delay of 5 seconds
        hasTimeElapsed = false
        initialCase = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            hasTimeElapsed = true
        }
    }
    
    func delayAllert2() {
        // Delay of 5 seconds
        hasTimeElapsed2 = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            hasTimeElapsed2 = true
        }
    }
    var body: some View {
        GeometryReader {geometry in
            ZStack {
                VStack{
                    Spacer()
                    switch current {
                    case "Initial":
                        IniInfo(click: $current, width: geometry.size.width)
                    case "Login":
                        Login(click: $current, width: geometry.size.width)
                    case "Signup":
                        Signup(click: $current, width: geometry.size.width)
                            .padding(.top, 50)
                    case "QuickMedi":
                        Player(select: $current, link: "", width: geometry.size.width, name: "Free Generated Meditation", textToSpeech: true)
                    default:
                        IniInfo(click: $current, width: geometry.size.width)
                    }
                    /*TabView(selection: $current) {
                     IniInfo(click: $current, width: geometry.size.width)
                     .tag("Initial")
                     Login(click: $current, width: geometry.size.width)
                     .tag("Login")
                     Signup(click: $current, width: geometry.size.width)
                     .tag("Signup")
                     }
                     .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                     .disabled(current != "Initial" ? true : false)
                     */
                }
                .background(Image("LoginBG")
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: geometry.size.width+10))
                .edgesIgnoringSafeArea(.all)
                VStack {
                    ZStack {
                        RoundedRectangle(cornerRadius: 20)
                            .foregroundColor(Color.black)
                            .opacity(0.95)
                        HStack {
                            Image(systemName: "exclamationmark.circle")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: geometry.size.width/10)
                                .foregroundColor(.white)
                            VStack(alignment: .leading, spacing: 0){
                                Text("You lost internet connection!")
                                    .font(.title3)
                                    .foregroundColor(Color.white)
                                Text("Please check your settings")
                                    .font(.body)
                                    .foregroundColor(Color.white)
                            }
                            .padding(.horizontal)
                        }
                    }
                    .frame(height: height/6)
                    .offset(x: 0, y: monitor.isConnected || hasTimeElapsed ? -300 : 0)
                    .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                    .padding()
                    .onReceive(monitor.$isConnected){ time in
                        DispatchQueue.main.async {
                            if !self.monitor.isConnected {
                                delayAllert()
                            }
                        }
                    }
                    Spacer()
                }
                VStack {
                    ZStack {
                        RoundedRectangle(cornerRadius: 20)
                            .foregroundColor(Color.black)
                            .opacity(0.95)
                        HStack {
                            Image(systemName: "exclamationmark.circle")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: geometry.size.width/10)
                                .foregroundColor(.white)
                            VStack(alignment: .leading, spacing: 0){
                                Text("Connection restored")
                                    .font(.title3)
                                    .foregroundColor(Color.white)
                            }
                            .padding(.horizontal)
                        }
                    }
                    .frame(height: height/6)
                    .offset(x: 0, y: monitor.isConnected && !hasTimeElapsed2 ? 0 : -300)
                    .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                    .padding()
                    .onReceive(monitor.$isConnected){ _ in
                        DispatchQueue.main.async {
                            if monitor.isConnected && initialCase{
                                delayAllert2()
                            }
                        }
                    }
                    Spacer()
                }
            }
        }
    }
}
