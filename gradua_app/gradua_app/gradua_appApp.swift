//
//  gradua_appApp.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 25/03/21.
//

import SwiftUI
import Firebase
import IQKeyboardManagerSwift

@main
struct gradua_appApp: App {
    
    /*
     Taking into account the new App Lifecycle of SwiftUI we need to override the main structure... There are more alternatives to override de old AppDelegate in https://stackoverflow.com/questions/62626652/where-to-configure-firebase-in-my-ios-app-in-the-new-swiftui-app-life-cycle-with
     */
    
    init() {
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done"
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(SessionStore())
                .environmentObject(AssistantManager())
                .environmentObject(NetworkMonitor())
                .environmentObject(GenMeditationsAPI())
        }
    }
}
