//
//  File.swift
//  gradua_app
//
//  Created by Isabela Sarmiento on 2/04/21.
//

import Foundation
import FirebaseFirestoreSwift

struct MeditationBack: Identifiable, Codable, Equatable {
    @DocumentID var id : String? = UUID().uuidString
    var label : String
    var link : String
    var reproductions : Int
}

