//
//  User.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 31/03/21.
//

import SwiftUI

struct User: Codable {
    var uid : String
    var email : String
    var displayName: String
    
    init(uid: String, displayName: String?, email: String?) {
        self.uid = uid
        self.displayName = displayName ?? ""
        self.email = email!
    }
    
}

