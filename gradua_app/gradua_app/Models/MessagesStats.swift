//
//  UserStats.swift
//  gradua_app
//
//  Created by Isabela Sarmiento on 4/06/21.
//

//
//  User.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 30/03/21.
//

import Foundation

struct MessagesStats: Codable {

    var positive_emotions: Int = 0
    var negative_emotions: Int = 0


    // Costructor vacio
    init() {
        self.positive_emotions = 0
        self.negative_emotions = 0
    }
    
    init(positive_emotions: Int?,negative_emotions: Int?) {
        self.positive_emotions = positive_emotions!
        self.negative_emotions = negative_emotions!
    }
    
    // Setter methods
    mutating func set_statistics(positive_emotions: Int?,negative_emotions: Int?){
        self.positive_emotions = positive_emotions!
        self.negative_emotions = negative_emotions!
    }
   
    mutating func add_positive (){
        print("positiva")
        print(self.positive_emotions)
        self.positive_emotions = self.positive_emotions + 1
        print(self.positive_emotions)
    }
    
    mutating func add_negative (){
        print("negativa")
        print(self.negative_emotions)
        self.negative_emotions = self.negative_emotions + 1
        print(self.negative_emotions)
    }
}
