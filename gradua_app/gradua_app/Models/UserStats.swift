//
//  UserStats.swift
//  gradua_app
//
//  Created by Isabela Sarmiento on 4/06/21.
//

//
//  User.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 30/03/21.
//

import Foundation
import FirebaseFirestoreSwift

struct UserStats: Codable {

    var num_meditaciones: Int
    var tiempo: Int
    
    var positive_emotions: Int
    var negative_emotions: Int
    
    var num_generadas: Int


    // Complete costructor
    init( num_meditaciones: Int?,tiempo: Int?,
         positive_emotions: Int?,negative_emotions: Int?,num_generadas: Int?) {
        self.tiempo = tiempo!
        self.num_meditaciones = num_meditaciones!
        self.positive_emotions = positive_emotions!
        self.negative_emotions = negative_emotions!
        self.num_generadas = num_generadas!
    }
    
    // Setter methods
    mutating func set_statistics (num_meditaciones: Int?,tiempo: Int?,
                         positive_emotions: Int?,negative_emotions: Int?,num_generadas: Int?){
        self.tiempo = tiempo!
        self.num_meditaciones = num_meditaciones!
        self.positive_emotions = positive_emotions!
        self.negative_emotions = negative_emotions!
        self.num_generadas = num_generadas!
    }
    
    mutating func add_num_meditaciones (){
        print("llega")
        print(self.num_meditaciones)
        self.num_meditaciones = self.num_meditaciones + 1
        print(self.num_meditaciones)

    }
    
    mutating func add_tiempo (){
        print("llega")
        print(self.tiempo)
        self.tiempo = self.tiempo + Int.random(in: 180..<600)
        print(self.tiempo)
    }
    
    mutating func add_generada (){
        print("generada")
        print(self.num_generadas)
        self.num_generadas = self.num_generadas + 1
        print(self.num_generadas)
    }
    
    mutating func add_positive (){
        print("positiva")
        print(self.positive_emotions)
        self.positive_emotions = self.positive_emotions + 1
        print(self.positive_emotions)
    }
    
    mutating func add_negative (){
        print("negativa")
        print(self.negative_emotions)
        self.negative_emotions = self.negative_emotions + 1
        print(self.negative_emotions)
    }
}
