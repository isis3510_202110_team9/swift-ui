//
//  SessionStore.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 30/03/21.
//

import SwiftUI
import Firebase
import Combine

class SessionStore : ObservableObject {
    @Published var session: User?
    @Published var profile: UserProfile?
    @Published var stats: UserStats?
    @Published var meditacion: MeditationBack?
    @Published var meditations : [MeditationBack] = []
    var handle: AuthStateDidChangeListenerHandle?
    
    var firestoreManager = FirestoreManager()
        
    func listen() {
        
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if let user = user {
                //If existing user then create it with the model user...
                self.session = User(uid: user.uid, displayName: user.displayName, email: user.email)
                self.firestoreManager.fetchProfile(userId: user.uid) { (profile, error) in
                    if let error = error {
                        print("Error while fetching the user profile: \(error)")
                    }
                    self.profile = profile
                }
                self.getMeditations(){(meditations, error) in
                    if let error = error
                    {
                        print(error)
                    }
                    else
                    {
                        self.meditations = meditations ?? []
                    }
                }
            } else {
                //No user, check local
                //Session
                if let localUserSession = UserDefaults.standard.data(forKey: "userSession") {
                    if let decodedUS = try? JSONDecoder().decode(User.self, from: localUserSession) {
                        print("Retrieving local session...")
                        self.session = decodedUS
                        //Profile
                        if let localUserProfile = UserDefaults.standard.data(forKey: "userProfile") {
                            if let decodedUP = try? JSONDecoder().decode(UserProfile.self, from: localUserProfile) {
                                self.profile = decodedUP
                                //A este nivel revisariamos los favoritos
                                
                                return
                            }
                        }
                    }
                }
                self.session = nil
                self.profile = nil
            }
        }
        )
        load()
    }
    
    
    func signUp(email: String, password: String, name: String, lastname: String, interests: Array<String>, completion: @escaping (_ profile: UserProfile?, _ error: Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print("Error signing up: \(error)")
                completion(nil, error)
                return
            }
            
            guard let user = result?.user else { return }
            print("User \(user.uid) signed up.")
            let userStats = UserStats(num_meditaciones: 0, tiempo: 0, positive_emotions: 0, negative_emotions: 0, num_generadas: 0)
            self.stats = userStats;
            
            let userProfile = UserProfile(id: user.uid, name: name, lastname: lastname, interests: interests)
            self.firestoreManager.createProfile(profile: userProfile) { (profile, error) in
                if let error = error {
                    print("Error while fetching the user profile: \(error)")
                    completion(nil, error)
                    return
                }
                self.profile = profile
                self.save()
                print("PROFILEEEEE")
                print(self.profile)
                completion(profile, nil)
            }
        }
    }
    
    func signIn(email: String, password: String, completion: @escaping (_ profile: UserProfile?, _ error: Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print("Error signing in: \(error)")
                completion(nil, error)
                return
            }
            
            guard let user = result?.user else { return }
            print("User \(user.uid) signed in.")
            
            self.firestoreManager.fetchProfile(userId: user.uid) { (profile, error) in
                if let error = error {
                    print("Error while fetching the user profile: \(error)")
                    completion(nil, error)
                    return
                }
                
                self.profile = profile
                self.save()
                completion(profile, nil)
            }
        }
    }
    
    
    func signOut () -> Bool {
        do {
            try Auth.auth().signOut()
            self.session = nil
            self.profile = nil
            self.deleteAll()
            return true
        } catch {
            return false
        }
    }
    
    func unbind() {
        if let handle = handle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
    
    deinit {
        unbind()
    }
    
    func getFavorites(completion: @escaping (_ meditations: [MeditationBack]?, _ error: Error?) -> Void) {
        
    }
    
    func getMeditation(name: String, completion: @escaping (_ meditation: MeditationBack?, _ error: Error?) -> Void) {
        
        self.firestoreManager.fetchMeditation(name: name){ (meditation, error) in
            if let error = error {
                print("Error while fetching the meditation: \(error)")
                completion(nil, error)
                return
            }
            
            self.meditacion = meditation!
            completion(meditation, nil)
        }
    }
    
    func getMeditations(completion: @escaping (_ meditations: [MeditationBack]?, _ error: Error?) -> Void) {
        
        self.firestoreManager.fetchMeditations(){ (meditations, error) in
            if let error = error {
                print("Error while fetching the meditation: \(error)")
                completion(nil,error)
                return
            }
            completion(meditations, nil)
        }
    }
    
    func updateMeditation(meditation : MeditationBack)
    {
        self.firestoreManager.updateMeditation(meditation)
    }
    
    func updateUserProfile(user : UserProfile) {
        self.firestoreManager.updateUserProfile(user)
    }
    
    func save() {
        print("Saving items")
        if let encodedUS = try? JSONEncoder().encode(self.session) {
            UserDefaults.standard.set(encodedUS, forKey: "userSession")
            print("Successfully Saved Session")
        }
        if let encodedUP = try? JSONEncoder().encode(self.profile) {
            UserDefaults.standard.set(encodedUP, forKey: "userProfile")
            print("Successfully Saved Profile")
        }
        if let encodedUS = try? JSONEncoder().encode(self.stats) {
            UserDefaults.standard.set(encodedUS, forKey: "userStats")
            print("Successfully Saved Stats")
        }
        //Aca guardariamos los usuarios
    }
    
    // cargar las estadísiticas
    func load() {
        if let data = UserDefaults.standard.data(forKey: "userStats") {
            print("Retrieving local stats data")
            if let decoded = try? JSONDecoder().decode(UserStats.self, from: data) {
                print("Loaded userStats are: \(decoded)")
                self.stats = decoded
                return
            }
        }
        print("THESE ARE THE MESSAGES AFTER STARTING \(String(describing: stats))")
    }
    
    func deleteAll() {
        if let bundleID = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundleID)
        }
    }
    
    func add_num_meditaciones (){
        self.stats?.add_num_meditaciones()
        self.save()
    }
    
    func add_tiempo (){
        self.stats?.add_tiempo()
        self.save()
    }
    
    func add_generada (){
        self.stats?.add_generada()
        self.save()
    }
    

    
}
