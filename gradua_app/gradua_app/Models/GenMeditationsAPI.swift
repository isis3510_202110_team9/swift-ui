//
//  GenMeditationsAPI.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 1/06/21.
//

import SwiftUI
import Combine

class GenMeditationsAPI : ObservableObject {
    
    @Published var meditacion : GeneratedMeditation?
    
    func getMeditation(username: String, completion: @escaping (_ meditation: GeneratedMeditation?, _ error : Error?) -> Void) {
        guard let url = URL(string: "http://www.gradua.cloud/meditations/random?name=\(username)") else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let err = error {
                print("There was an error retrieving the generated meditation from API: \(err)")
                completion(nil, error)
                return
            } else {
                let meditation = try! JSONDecoder().decode(GeneratedMeditation.self, from: data!)
                //Allow us to play with the app while the call is being made.
                DispatchQueue.main.async {
                    var profiled = meditation
                    let str = profiled.meditation.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                    profiled.meditation = str
                    print("Resulting string: \(str)")
                    self.meditacion = profiled
                    completion(profiled, nil)
                    print("Generated meditation retrieved. ")
                }
            }
        }
        .resume()
    }
}
