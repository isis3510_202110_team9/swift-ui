//
//  FirestoreManager.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 31/03/21.
//

import SwiftUI
import Firebase
import FirebaseFirestoreSwift

class FirestoreManager: ObservableObject {
    
    @Published var user = ""
    @Published var meditation = ""
    @Published var meditations : [MeditationBack] = []
    var db = Firestore.firestore()
    
    func fetchProfile(userId: String, completion: @escaping (_ profile: UserProfile?, _ error: Error?) -> Void) {
        db.collection("users").document(userId).getDocument { (snapshot, error) in
            let profile = try? snapshot?.data(as: UserProfile.self)
            completion(profile, error)
        }
    }
    
    func createProfile(profile: UserProfile, completion: @escaping (_ profile: UserProfile?, _ error: Error?) -> Void) {
        do {
            let _ = try db.collection("users").document(profile.id!).setData(from: profile)
            completion(profile, nil)
        }
        catch let error {
            print("Error writing user to Firestore: \(error)")
            completion(nil, error)
        }
    }
    
    func fetchMeditation(name: String, completion: @escaping (_ name: MeditationBack?, _ error: Error?) -> Void) {
        
        db.collection("meditations").document(name).getDocument() { (snapshot, error) in
            let meditation = try? snapshot?.data(as: MeditationBack.self)
            completion(meditation, error)
        }
    }
    
    
    func fetchMeditations(completion: @escaping (_ name: [MeditationBack], _ error: Error?) -> Void){
        db.collection("meditations").getDocuments(){ (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.meditations = querySnapshot?.documents.compactMap {document in
                    try?document.data(as: MeditationBack.self)
                } ?? []
                
                completion(self.meditations,err)
            }
        }
    }
    
    func updateMeditation(_ meditation : MeditationBack)
    {
        if let uid = meditation.id
        {
            do{
                try db.collection("meditations").document(uid).setData(from: meditation)
            }
            catch
            {
                print(error)
            }
        }
    }
    
    func updateUserProfile(_ user : UserProfile) {
        if let uid = user.id {
            do {
                try db.collection("users").document(uid).setData(from: user)
            } catch {
                print(error)
            }
        }
    }
}
