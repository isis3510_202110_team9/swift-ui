//
//  User.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 30/03/21.
//

import Foundation
import FirebaseFirestoreSwift

struct UserProfile: Identifiable, Codable {
    @DocumentID var id : String? = UUID().uuidString
    var name : String
    var lastname : String
    var interests : Array<String>
    var favorites : Array<String> = []
    

}
