//
//  GeneratedMeditation.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 1/06/21.
//

import SwiftUI

struct GeneratedMeditation: Codable {
    var meditation : String
    var code : String
}
