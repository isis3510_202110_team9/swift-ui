//
//  AssistantManager.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 2/04/21.
//

import Foundation
import SwiftUI
import Assistant
import Combine

struct Msg: Codable {
    var content: String
    var role: Bool
    var date: Date
    
    init(content: String, role: Bool) {
        self.content = content
        self.role = role
        self.date = Date()
    }
    
}

class AssistantManager : ObservableObject {
    
    @Published var assistant = Assistant(version: "2020-09-24", authenticator: WatsonIAMAuthenticator(apiKey: "-7qGt134DxIhzeiJ9PEzWyeQ6b5-XwIttSJPtpUnNbtk"))
    var workspaceID = "24786ff9-f348-41cd-b7a5-09555a626dac"
    
    //Chat side array
    @Published var messages : [Msg] = []
    
    @Published var messagesStats : MessagesStats

    //Meditation link name
    @Published var meditationLink : String = ""
    @Published var meditationName : String = ""
    var context: Context?

    init() {
        if let data = UserDefaults.standard.data(forKey: "savedMessages") {
            print("Retrieving local message data")
            if let decoded = try? JSONDecoder().decode([Msg].self, from: data) {
                print("Loaded messages are: \(decoded)")
                self.messages = decoded
            }
        }
        self.messagesStats = MessagesStats()
        if let data = UserDefaults.standard.data(forKey: "messagesStats") {
            print("Retrieving local message data")
            if let decoded = try? JSONDecoder().decode(MessagesStats.self, from: data) {
                print("Loaded messages stats are: \(decoded)")
                self.messagesStats = decoded
            }
        }
        initialMessage()
    }
    
    func initialMessage() {
        // Start a conversation
        assistant.message(workspaceID: workspaceID) { response, error in
            if let error = error {
                print("ERRRROOOOOR")
                print(error)
            }
            
            guard let message = response?.result else {
                print("Failed to get the message.")
                print("ERRRROOOOOR")
                return
            }
            
            self.messages.append(Msg(content: message.output.text.joined(), role: false))
            self.save()
            print("Conversation ID: \(message.context.conversationID!)")
            print("Initial Message: \(message.output.text.joined())")
            print("LLEGUEEE")

            self.context = message.context
        }
        
    }
    
    // Start a conversationquick
    func message(userInput: String) {
        let input = MessageInput(text: userInput)
        assistant.message(workspaceID: workspaceID, input: input, context: self.context) {
            response, error in
            
            guard let message = response?.result else {
                print(error?.localizedDescription ?? "unknown error")
                return
            }
            print("CONTEXT")
            print(message)
            print("ENTITIES")
            print(message.entities)
            let entitiesDetected = message.entities
            for i in 0..<entitiesDetected.count {
                            if (entitiesDetected[i].entity == "NegativeEmotion"){
                                self.add_negative()
                            }
                            else if (entitiesDetected[i].entity == "PositiveEmotion"){
                                self.add_positive()
                            }
                        }
            
            print("MENSAJEEE")
            self.context = message.context
            
            let modified = message.output.text.joined()
            
            self.messages.append(Msg(content: userInput, role: true))
            self.save()
            if modified.contains("{")
            {
                let data = Data(modified.utf8)
                do {
                    // make sure this JSON is in the format we expect
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        // try to read out a string array
                        if let link = json["file"] as? String {
                            self.meditationLink = link
                        }
                        if let name = json["responseType"] as? String {
                            self.meditationName = name
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            } else {
                self.messages.append(Msg(content: modified, role: false))
                self.save()
            }
        }
    }
    
    func noConnection(){
        self.messages.append(Msg(content: "You have no conectivity! I'm really sorry but I can't help you right now", role: false))
//        self.messages.append(Msg(content: "Currently you can only listen to your downloaded meditations", role: false))
    }
    
    func connection(){
        self.messages.append(Msg(content: "I'm back! Let's continue", role: false))
    }
    
    func save() {
        if let encoded = try? JSONEncoder().encode(self.messages) {
            UserDefaults.standard.set(encoded, forKey: "savedMessages")
            print("saved messagges successfully")
        }
        if let encoded = try? JSONEncoder().encode(self.messagesStats) {
            UserDefaults.standard.set(encoded, forKey: "messagesStats")
            print("saved messagges stats successfully")
        }
    }
    
    func add_positive (){
        self.messagesStats.add_positive()
        self.save()
    }
    
    func add_negative (){
        self.messagesStats.add_negative()
        self.save()
    }
    
}
