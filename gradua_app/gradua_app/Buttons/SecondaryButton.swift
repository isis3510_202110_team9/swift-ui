//
//  SecondaryButton.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI

struct SecondaryButton: View {
    
    var text : String
    var width : CGFloat
    
    @EnvironmentObject var monitor : NetworkMonitor

    var body: some View {
        Text(text)
            .frame(width: width)
            .font(.body)
            .foregroundColor(monitor.isConnected ? Color("DarkPurple") : Color("Grey"))
            .padding()
            .overlay(
                RoundedRectangle(cornerRadius: 15).stroke(monitor.isConnected ? Color("DarkPurple") : Color("Grey"), lineWidth: 1))
    }
}
