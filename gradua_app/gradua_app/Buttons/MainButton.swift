//
//  MainButton.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI

struct MainButton: View {
    
    var text : String
    var width :  CGFloat

    @EnvironmentObject var monitor : NetworkMonitor

    var body: some View {
        Text(text)
            .frame(width: width)
            .font(.body)
            .foregroundColor(Color.white)
            .padding()
            .background(monitor.isConnected ? Color("DarkPurple") : Color("Grey"))
            .opacity(/*@START_MENU_TOKEN@*/0.8/*@END_MENU_TOKEN@*/)
            .cornerRadius(15)
    }
}
