//
//  Chatbot.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI

struct ChatBot: View {
    
    @Binding var selection : String
    @Binding var link : String
    @Binding var meditationName : String
    @Binding var showChat : Bool
    
    @State var width : CGFloat
    @State var textField = ""
    @EnvironmentObject var assistant : AssistantManager
    @EnvironmentObject var session : SessionStore
    @EnvironmentObject var monitor : NetworkMonitor
    
    @State var initial = false
    //@State var meditation : MeditationBack = MeditationBack(id: "", label: "", link: "", reproductions: 0)
    
    /*
     func getMeditation(name: String) {
     session.getMeditation(name: name) {(meditation, error) in
     self.meditation = meditation!
     }
     }
     */
    
    func updateMeditation(meditation : MeditationBack)
    {
        session.updateMeditation(meditation: meditation)
    }
    
    func formatDate(date : Date) -> String{
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let day = calendar.component(.day, from: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: date)
        let date_string = String(day) + " " + String(nameOfMonth) + " " + String(hour) + ":" + String(minutes)
        return date_string
    }
    
    var body: some View {
        VStack(spacing: 20) {
            Rectangle()
                .frame(width: 50, height: 5)
                .cornerRadius(3)
                .opacity(0.1)
            //Heading
            HStack {
                VStack(alignment: .leading) {
                    Text("GradúaBot")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                    Text("Ask me anything!")
                        .font(.headline)
                }
                .foregroundColor(.black)
                Spacer()
            }
            .padding(.top)
            .padding(.horizontal)
            
            //Chat
            ScrollView {
                LazyVStack {
                    ScrollViewReader { value in
                        ForEach(0 ..< assistant.messages.count, id:\.self) {index in
                            Text(formatDate(date: assistant.messages[index].date))
                            Message(mContent: assistant.messages[index].content, currentUser: assistant.messages[index].role)
                                .onAppear(perform: {
                                    if assistant.meditationLink != "" {
                                        selection = "Player"
                                        link = assistant.meditationLink
                                        meditationName = assistant.meditationName
                                        assistant.initialMessage()
                                    }
                                    assistant.meditationLink = ""
                                })
                        }.onAppear {
                            value.scrollTo( assistant.messages.count - 1)
                        }
                    }
                }
            }
            .padding(.top)
            .frame(maxHeight: .infinity)
            
            
            HStack {
                TextField("Message...", text: $textField)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.horizontal)
                    .frame(minHeight: CGFloat(30))
                    .disabled(monitor.isConnected == false)
                Button(action: {
                    assistant.message(userInput: textField)
                    textField = ""
                }) {
                    ZStack(alignment: .center) {
                        RoundedRectangle(cornerRadius: 8)
                            .frame(width: 40, height: 40)
                            .foregroundColor(monitor.isConnected ? Color("ChatBubble") : Color("Grey"))
                        Image(systemName: "paperplane.fill")
                            .foregroundColor(.white)
                    }
                }
                .disabled(monitor.isConnected == false)
            }
            .padding(.bottom, 60)
            .frame(minHeight: CGFloat(50))
            
            Spacer()
        }
        .padding(.vertical, 8)
        .padding(.horizontal, 20)
        .frame(maxWidth: .infinity)
        .background(Color.white)
        .cornerRadius(30)
        .shadow(radius: 20)
        .onReceive(monitor.$isConnected){ _ in
            DispatchQueue.main.async {
                if !monitor.isConnected && showChat {
                    assistant.noConnection()
                    self.initial = true
                }
                if monitor.isConnected && self.initial && showChat{
                    assistant.connection()
                    self.initial = false
                }
            }
        }
        .onChange(of: showChat){ _ in
            if showChat {
                assistant.initialMessage()
            }
        }
    }
}
