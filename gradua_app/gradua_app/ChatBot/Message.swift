//
//  Message.swift
//  gradua_app
//
//  Created by Luis Miguel Gómez Londoño on 29/03/21.
//

import SwiftUI

struct Message: View {
        
    var mContent : String
    //True si es el usuario, false si es el bot
    var currentUser : Bool
        
    var body: some View {
        HStack(alignment: .bottom, spacing: 15) {
            
            //false, el logo de gradua
            if !currentUser {
                Image("Logo")
                    .resizable()
                    .frame(width: 40, height: 40, alignment: .center)
                    .cornerRadius(20)
            }
            else {
                Spacer()
            }
            
            Text(mContent)
                .padding(10)
                .foregroundColor(currentUser ? Color.white : Color.black)
                .background(currentUser ? Color("ChatBubble") : Color(UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)))
                .cornerRadius(20)
            if !currentUser {
                Spacer()
            }
        }
    }
}
